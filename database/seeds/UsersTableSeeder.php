<?php

namespace Seeds;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run() {
        $user = User::where('name', 'admin')->first();
        if(!$user) {
            $user = new User(['name' => 'admin', 'email'=>'forbusy@yahoo.com', 'password'=>Hash::make('changeme')]);
            $user->save();
        } else {
            echo 'user admin already exist';
        }
    }
}
